let
  sources = import ./sources.nix;
  config = {};
  pkgs = import sources.nixpkgs {};
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    nodePackages.pnpm
    nodePackages.postcss-cli
    sassc
  ];

  NODEPROG = "pnpm";
}
