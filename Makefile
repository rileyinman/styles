#!/usr/bin/make -f

MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += -j

.ONESHELL:

.DEFAULT_GOAL := all

.DELETE_ON_ERROR:

NODEOPTIONS ?= --no-update-notifier
NODEPROG ?= npm
NODECMD = $(NODEPROG) $(NODEOPTIONS)

cssDir := css
sassDir := scss
chromeDir := userChrome
contentDir := userContent

dirs := \
	$(cssDir)/$(chromeDir) \
	$(cssDir)/$(contentDir)

userChrome := $(wildcard $(sassDir)/$(chromeDir)/*.scss)
userContent := $(wildcard $(sassDir)/$(contentDir)/*.scss)

.PHONY: test
test:
	$(info userChrome listing: $(userChrome))
	$(info userContent listing: $(userContent))

$(dirs): %:
	mkdir -p $@

node_modules: package.json
	$(NODECMD) install

all: userchrome usercontent

userchrome: $(userChrome:scss/%.scss=css/%.css)

usercontent: $(userContent:scss/%.scss=css/%.css)

css/userContent/%.css: scss/userContent/%.scss $(dirs) node_modules
	sassc $< $@
	postcss -o $@ $@

css/userChrome/%.css: scss/userChrome/%.scss $(dirs) node_modules
	sassc $< $@
	postcss -o $@ $@
